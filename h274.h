#include <libavutil/film_grain_params.h>

// Must be initialized to {0} prior to first usage
typedef struct H274FilmGrainDatabase {
    // Database of film grain patterns, lazily computed as-needed
    uint16_t residency[13 /* h */]; // bit field of v
    int8_t db[13 /* h */][13 /* v */][64][64];

    // Temporary buffer for slice generation
    int16_t slice_tmp[64][64];
} H274FilmGrainDatabase;

// Synthesizes film grain on top of `in` and stores the result to `out`.
//
// Returns a negative error code on error, such as invalid params.
int h274_apply_film_grain(AVFrame *out, const AVFrame *in,
                          H274FilmGrainDatabase *db,
                          const AVFilmGrainParams *params);
