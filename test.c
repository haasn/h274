#include "h274.h"

static const AVFilmGrainParams params =
{
    .type = AV_FILM_GRAIN_PARAMS_H274,
    .seed = 0, // TODO
    .codec.h274 = {
        0 // TODO
    },
};

int main()
{
    // Allocate mock AVFrame
    AVFrame *src, *dst;
    src = av_frame_alloc();
    dst = av_frame_alloc();
    if (!src || !dst)
        exit(1);

    src->format = dst->format = AV_PIX_FMT_YUV420P;
    src->width = dst->width = 1920;
    src->height = dst->height = 1080;

    if (av_frame_get_buffer(src, 0) || av_frame_get_buffer(dst, 0))
        exit(1);

    // TODO: load source frame into src->data

    static H274FilmGrainDatabase db = {0};
    if (h274_apply_film_grain(dst, src, &db, &params))
        exit(2);

    // TODO: output dst frame

    av_frame_free(&src);
    av_frame_free(&dst);
    return 0;
}
